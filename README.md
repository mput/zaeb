# Решение домашних заданий жанга
![pipeline](https://gitlab.crja72.ru/django_2023/students/163088-putilovskaya-47329/badges/main/pipeline.svg)

#Активация виртуального окружения

```bash
python-ven venv
source venv/bin/activate
```

#Установка зависимостей

```bash
pip3 install -r requirements/prod.txt
```

#Для разработки необходимо дополнительно установить зависимости из requirements/dev.txt bash

```bash
pip3 install -r requirements/dev.txt
```

#Для запуска тестов зависимости перечислены в requirements/test.txt"

```bash
 pip3 install requirements/test.txt
 ```

#Настройка переменных окружения

Скопируйте файл ```config.env``` в ```.env``` если нужно, отредактируйте значения переменных.

```bash
cp config.env .env.
```

#Запуск

```bash
python3 manage.py runserver
```