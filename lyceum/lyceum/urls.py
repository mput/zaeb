from django.contrib import admin
from django.urls import path, include, re_path

import lyceum

urlpatterns = [
    path("", include("homepage.urls")),
    path("", include("catalog.urls")),
    path("", include("about.urls")),
    path("admin/", admin.site.urls),
]
if lyceum.settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        re_path(r"^__debug__/", include(debug_toolbar.urls)),
    ]
