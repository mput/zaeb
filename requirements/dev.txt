Django==4.2.5
typing_extensions==4.8.0
tzdata==2023.3
python-dotenv==1.0.0
django-debug-toolbar==4.2.0
black==23.9.1